const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require('../auth');

const {verify, verifyAdmin} = auth;


router.post('/', verify, verifyAdmin, productControllers.addProduct);

router.get('/', productControllers.getAllProducts);

router.get('/getSingleProduct/:id', productControllers.getSingleProduct);

router.put("/archive/:id", verify, verifyAdmin, productControllers.archiveProduct);

router.put("/activate/:id", verify, verifyAdmin, productControllers.activateProduct);

router.get("/getActiveProducts", productControllers.getActiveProducts);


router.put("/updateProduct/:id", verify, verifyAdmin, productControllers.updateProduct);


router.get("/getInactiveProducts", verify, verifyAdmin, productControllers.getInactiveProducts);


router.post("/findProductsByName", productControllers.findProductsByName);


router.post("/findProductsByPrice", productControllers.findProductsByPrice);

router.get("/getOrders/:id", verify, verifyAdmin, productControllers.getOrders);

module.exports = router;
