const express = require("express");
const router = express.Router();

//import user controllers
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// routes

// User Registration
router.post('/', userControllers.registerUser);

// Get all Users
router.get('/', userControllers.getAllUsers);

// Login Route
router.post("/login", userControllers.loginUser);

// Retrieve User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails);

router.post("/checkEmailExists", userControllers.checkEmailExists);

// Updating a regular user to admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

// Enroll registered user
router.post("/order", verify, userControllers.Order);

router.get("/getMyOrder", verify, userControllers.getMyOrder);

module.exports = router;

