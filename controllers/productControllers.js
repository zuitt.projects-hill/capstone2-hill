const Product = require("../models/Product");


module.exports.addProduct = (req, res) => {

	console.log(req.body);

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newProduct.save()
	.then(course => res.send(course))
	.catch(error => res.send(error));
};

// Retrieve All Products

module.exports.getAllProducts = (req, res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.getSingleProduct = (req, res) => {

	console.log(req.params);

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.archiveProduct = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error));
};


module.exports.activateProduct = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error));
};


module.exports.getActiveProducts = (req, res) => {

	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.updateProduct = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error));
};


module.exports.getInactiveProducts = (req, res) => {

	Product.find({isActive:false})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.findProductsByName = (req, res) => {

	Product.find({name: {$regex: req.body.name, $options:`$i`}})
	.then(result => {
		if(result.length === 0){
			return res.send("No products found");
		} else {
			return res.send(result);
		}
	})
	.catch(error => res.send(error));
};


module.exports.findProductsByPrice = (req, res) => {

	Product.find({price: req.body.price})
	.then(result => {

		if(result.length === 0){
			return res.send("No products found");

		} else {
			return res.send(result);
		}
	})
	.catch(error => res.send(error));
	};


module.exports.getOrders = (req, res) => {

	Product.findById(req.params.id)
	.then(result => {
		res.send(result.items)})
	.catch(error => res.send(error));
};
