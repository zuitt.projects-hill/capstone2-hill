
const bcrypt = require("bcrypt");

const User = require("../models/User");

const Product = require("../models/Product")

const auth = require("../auth");

// User Registration

module.exports.registerUser = (req, res) => {

	console.log(req.body);

	const hashedPW = bcrypt.hashSync(req.body.password, 10);


	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error));
};

// Retrieve All Users

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.loginUser = (req, res) => {

	console.log(req.body);


	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){

			return res.send("User does not exist");

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Password is incorrect.")
			}
		}
	})
	.catch(err => res.send(err));
};


module.exports.getUserDetails = (req, res) => {

	console.log(req.user)

	// 1. find a logged in users document from our database and send it to the client by its ID

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.checkEmailExists = (req, res) => {

	User.findOne({email: req.body.email})
		.then(foundEmail => {

			if(foundEmail === null){

				return res.send("This Email is Available");

			} else {

					return res.send("This Email is already registered.")
				}
			})
		.catch(error => res.send(error));
	
};

// Updating a regular user to Admin

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};

// Update User Details

module.exports.updateUserDetails = (req, res) => {

	console.log(req.body); // checking the input for new values for our users details
	console.log(req.user.id); //check the logged in user

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};


module.exports.Order = async (req, res) => {


	console.log(req.user.id);
	console.log(req.body.productId);


	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		console.log(user);

		// add the courseId in an object and push that object into user's enrollment array
		let newCart = {
			productId: req.body.productId
		}

		user.cart.push(newCart);

		return user.save()
		.then(user => true)
		.catch(error => error.message);
	})

	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message.

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isProductUpdated = await Product.findById(req.body.productId)
	.then(product => {

		console.log(product);

		let items = {
			userId: req.user.id
		}

		product.item.push(items);

		return product.save()
		.then(product => true)
		.catch(error => error.message);

	})

	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated})
	}

	if(isUserUpdated && isProductUpdated){
		return res.send({message: 'Ordered Successfully!'})
	}


};


module.exports.getMyOrder = (req, res) => {

	User.findById(req.user.id)
	.then(result => {res.send(result.cart)})
	.catch(error => res.send(error))
};
