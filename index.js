
const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

const port = 4000;


const app = express();

mongoose.connect("mongodb+srv://Admin_Hill:admin169@batch-169.jbeqs.mongodb.net/Capstone2?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
		
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));


app.use(express.json());

app.use(cors());

app.use('/products', productRoutes);

app.use('/users', userRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));