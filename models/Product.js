const mongoose = require("mongoose");


let productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product Name is required"]
	},

	description: {
		type: String,
		required: [true, "Product description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	item: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},

			totalAmount: {
				type: Number,
				required: [true, "Total Required"]
			},

			dateOrdered: {
				type: Date,
				default: new Date()
			}
			
		}
	]

})


module.exports = mongoose.model("Product", productSchema);
