const mongoose = require("mongoose");


let userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [false, "First Name"]
	},

	lastName: {
		type: String,
		required: [false, "Last Name"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "First Name is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	cart: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},

			totalAmount: {
				type: Number,
				required: [true, "Total Amount is required"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
			
		}
	]

})


module.exports = mongoose.model("User", userSchema);
